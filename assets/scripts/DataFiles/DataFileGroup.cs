﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DataFileGroup
{
	/***********************************************
	 * 
	 * Contains the different related files (generated
	 * from the same simulation)
	 * 
	 **********************************************/
	
	// Container for the path to each file
	public Dictionary<string,string> filePaths = new Dictionary<string,string> ();
	
	// These are used when the files are read
	public bool isLoaded = false;
	public DataFile file;
	
	// Constructor
	public DataFileGroup (string filePath, string extTwo)
	{
		filePaths.Add (extTwo, filePath);
	}
}