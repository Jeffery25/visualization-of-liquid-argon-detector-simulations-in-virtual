﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Syntax
{
	/***********************************************
	 * 
	 * Specifies the syntax of files outputed by LarSoft
	 * and loaded by DataFileLoader
	 * 
	 **********************************************/

	// Here are compiled the list of different types of files with their varying syntax
	public static readonly List<string> extSecond = new List<string> () 
	{
		"particles",
		"charge",
	};

	// Location of data files with wanted extensions
	public static readonly string dataFolder = @"Assets/Larsoft_files/";
	public static readonly string extFile = "*.dat";

	// Unit Line at top
	// Make it possible to recognize units, and make dictionary below
	// For now the first line is ignored

	// Tokens in data files
	public static readonly string eventToken = "Event";
	public static readonly string particleToken = "Particle type";
	public static readonly string splitToken = ": ";
	public static readonly string splitData = " ";

	// Number and order of coulumns
	public static readonly int numColumns = 5;
	public static readonly int x = 0;
	public static readonly int y = 1;
	public static readonly int z = 2;
	public static readonly int timeStamp = 3;
	public static readonly int energy = 4;
	public static readonly int charge = 4;
}
