﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class DataFileLoader : MonoBehaviour
{
	/***********************************************
	 * 
	 * First finds all available .dat files, then
	 * reads them, creating the corresponding 
	 * DataFile classes.
	 * 
	 **********************************************/
	
	// Create dictionary of available data files; basename => DataFileGroup class
	public Dictionary<string, DataFileGroup> dataFilesDict = new Dictionary<string, DataFileGroup>();

	void OnEnable () 
	{
		// File name variables, for finding the paths to all relevant files
		string name;
		string[] namePieces;
		string extTwo;
		string baseName;

		// Find all data files in folder specified in Syntax.cs
		foreach (string dataFile in Directory.GetFiles (Syntax.dataFolder, Syntax.extFile, SearchOption.AllDirectories)) 
		{
			// Get name of file, and extensions. 
			// Note if there is a  / in the name the file the name will be clipped
			name = dataFile.Substring (dataFile.LastIndexOf ("/") + 1);
			namePieces = name.Split ('.');

			// Check that there are at least two extensions
			if (namePieces.Length < 3) 
			{
				ErrorFileName (dataFile);
			}
			else
			{
				// find second to last extension name and base name
				extTwo = namePieces [namePieces.Length - 2];
				baseName = name.Substring (0, name.LastIndexOf (extTwo) - 1);

				// Check that the last and second to last extensions are correct
				if (namePieces [namePieces.Length - 1] != "dat" || !Syntax.extSecond.Contains (extTwo)) 
				{
					ErrorFileName (dataFile);
				} 
				else 
				{
					// Add in dictionary: create new class if new simulation name,
					// in existing class if related file was already added
					if (!dataFilesDict.ContainsKey (baseName)) 
					{
						dataFilesDict.Add (baseName, new DataFileGroup (dataFile, extTwo));
					} 
					else 
					{
						// make sure no other file has the same name
						if (!dataFilesDict [baseName].filePaths.ContainsKey (extTwo)) 
						{
							dataFilesDict [baseName].filePaths [extTwo] = dataFile;
						} 
						else 
						{
							Debug.LogError ("There are several instances of the file " + name);
						}
					}
				}
			}
		}

		// Make sure there aren't files missing
		foreach (KeyValuePair<string, DataFileGroup> entry in dataFilesDict) 
		{
			if (entry.Value.filePaths.Count != Syntax.extSecond.Count) 
			{
				Debug.LogError ("Expected file(s) with base name '" + entry.Key + "' missing.");
			}
		}
		
		// File reading variables, to read the relevant files
		string line;
		int index = 0;
		string filePath;
		
		List<int>[] eventIndexes = new List<int>[Syntax.extSecond.Count];
		List<int>[] particleIndexes = new List<int>[Syntax.extSecond.Count];
		List<string>[] linesCompil = new List<string>[Syntax.extSecond.Count];
		
		List<int> eventIndex;
		List<int> particleIndex;
		List<string> lines;

		// Temporary, need to make downloading file content optional
		// Now iterates through all paths
		foreach (KeyValuePair<string, DataFileGroup> dataGroup in dataFilesDict) 
		{	
			
			for (int num = 0; num < Syntax.extSecond.Count; num++)
			{
				filePath = dataGroup.Value.filePaths [Syntax.extSecond[num]];
				index = 0;
				
				eventIndex = new List<int> (); 
				particleIndex = new List<int> (); 
				lines = new List<string> (); 
				
				// reading the file 
				using (StreamReader file = new StreamReader(filePath)) 
				{	
					while (!file.EndOfStream) 
					{
						// add line to list of lines
						line = file.ReadLine ();
						lines.Add (line);
						
						// check for tokens
						if (line.StartsWith (Syntax.eventToken)) 
						{
							eventIndex.Add (index);
						}
						if (line.StartsWith (Syntax.particleToken)) 
						{
							particleIndex.Add (index);
						}

						index++;
					}
					
					file.Close ();
				}
				
				eventIndexes[num] = eventIndex;
				particleIndexes[num] = particleIndex;
				linesCompil[num] = lines;
			}
			
			// check that the file contains events, and create a DataFile instance
			if (eventIndexes[0].Count > 0) 
			{
				dataGroup.Value.file = new DataFile (linesCompil, eventIndexes, particleIndexes);
			} 
			else 
			{
				Debug.LogError ("The file " + dataGroup.Value.filePaths [Syntax.extSecond[0]] + " contains no events." +
				                " They should be flagged by the token '" + Syntax.eventToken + "'.");
			}
			
			// signal loading
			dataGroup.Value.isLoaded = true;
		}

	}

	public static void ErrorFileName (string dataFile)
	{
		Debug.LogError ("The file name " + dataFile + " must be written as 'basename.type.dat', " +
		                "where type is one of: '" + string.Join("', '", Syntax.extSecond.ToArray()) + "'.");
	}

}