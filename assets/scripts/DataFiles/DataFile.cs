﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DataFile
{
	/***********************************************
	 * 
	 * Contains the events listed in one data file
	 * 
	 **********************************************/
	
	public readonly List<Event> events = new List<Event> ();
	
	// Constructor
	public DataFile (List<string>[] linesCompil, List<int>[] eventIndexes, List<int>[] particleIndexes)
	{
		int types = Syntax.extSecond.Count;
		int eventCount = eventIndexes[0].Count;
		bool error = false;
		
		if (linesCompil.Length != types || eventIndexes.Length != types || particleIndexes.Length != types)
		{
			error = true;
			Debug.LogError ("DataFile class constructor given arrays with incorrect dimensions.");
		}
		for (int i = 0; i < types - 1; i++)
		{
			 if (eventIndexes[i].Count != eventIndexes[i + 1].Count)
			 {
				error = true;
			 	Debug.LogError ("The event number in two files does not match.");
			 }
		}
		// if there is no errors in data dimensions, construct the events
		if (!error)
		{
			// indexing of particles within an event and the ines within that event
			// indexing order: event, type number, list
			List<int>[,] subParticleIndexes = new List<int>[eventCount, types];
			List<string>[,] linesRange = new List<string>[eventCount, types];
			
			// index beginning and end of an event in the lines
			int begin;
			int end;
			
			// holders of data
			List<int> eventIndex;
			List<int> particleIndex;
			List<string> lines;
			List<int> subParticleIndex = new List<int> ();
			
			// iterate through the different file types
			for (int typeNum = 0; typeNum < types; typeNum++)
			{
				// assign the data to temporary lists
				eventIndex = eventIndexes[typeNum];
				particleIndex = particleIndexes[typeNum];
				lines = linesCompil[typeNum];
				
				// add length to avoid breaking following loop
				eventIndex.Add (lines.Count);
	
				// iterate through the different events
				for (int eventNum = 0; eventNum < eventIndex.Count - 1; eventNum++) 
				{
					// avoid searching for these values several times
					// +1 to begin to avoid writing the line with event token
					begin = eventIndex[eventNum] + 1;
					end = eventIndex[eventNum + 1];
		
					// modify particleIndex so that it can still be used for indexing 
					// particle tokens within the sublist created with the event
					for (int j = 0; j < particleIndex.Count; j++)
					{
						if (particleIndex[j] >= end)
						{
							break;
						}
						if (particleIndex[j] >= begin)
						{
							subParticleIndex.Add (particleIndex[j] - begin);
						}
					}
					
					// assign data
					subParticleIndexes [eventNum, typeNum] = new List<int> (subParticleIndex);
					linesRange [eventNum, typeNum] = new List<string> (lines.GetRange (begin, end - begin));
					
					subParticleIndex.Clear ();
				}
			}
			
			// lists that hold the same data without the first dimension (one per event)
			List<int>[] eventSubParticleIndexes = new List<int>[types];
			List<string>[] eventLinesRange = new List<string>[types];
			
			// create the events
			for (int i = 0; i < eventCount; i++)
			{
				// reduce subParticleIndexes and linesRange down one dimension 
				// and initiate an event class for each
				for (int j = 0; j < types; j++)
				{
					eventSubParticleIndexes[j] = subParticleIndexes[i,j];
					eventLinesRange[j] = linesRange[i,j];
				}
				
				events.Add (new Event (eventLinesRange, eventSubParticleIndexes));
			}
		}
	}	
}