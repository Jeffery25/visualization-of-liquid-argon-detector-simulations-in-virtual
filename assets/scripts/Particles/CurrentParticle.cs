﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using ProceduralToolkit;

public class CurrentParticle : Particle
{
	/***********************************************
	 * 
	 * Particles contained in CurrentEvent class.
	 * They are copies of the data holder class Particle,
	 * but unwanted particles and data points are removed,
	 * plus the particle can create a meshes, and show itself
	 * 
	 **********************************************/
	
	// list referencing the several objects representing particle information
	private List<GameObjectComponents> objects = new List<GameObjectComponents> ();
			
	// notable Game objects
	private GameObject container;
	private GameObjectComponents animation;
	
	// animation states to play the animation
	public enum AnimParticle
	{
		Waiting,
		Starting,
		Playing,
		Paused,
		Done,
	}
	
	// used for iteration in coroutine
	private AnimParticle _state; // used for property below
	private AnimParticle prevState; // keeps track of previous state
	private int positionNum; // position of particle in animation

	// Properties
	// check time left before next position in case it is negative (go directly to next frame)
	private float timeLeft
	{
		get { return Point[positionNum].TimeStamp / CurrentEvent.Instance.Speed - CurrentEvent.Instance.time; }
	}
	// state (updates prevState automatically, reset using the ResetState function)
	public AnimParticle state
	{
		get { return _state; }
		set 
		{ 
			prevState = _state;
			_state = value; 
		}
	}
	// resets state to previous value, used when unpausing an animation
	public void ResetState ()
	{
		_state = prevState;
	}
	
	// Constructor to assign the values of the particle copy
	public CurrentParticle (Particle particleCopy, Filter cullFilter)
	{
		// change point so that the filter can apply
		// note that particleCopy is a shallow copy, hence lists / dictionnaries
		// must be recreated in order to leave the original untouched
		_point = new List<DataPointTemp> (particleCopy.Point);
		_type = particleCopy.Type;
		_name = particleCopy.Name;
		
		if (particleCopy.Charge != null)
		{
			_charge = new List<DataChargeTemp> (particleCopy.Charge);
		}
		
		// if some lines are uninteresting (i.e the particle doesn't move), remove them
		// defined in the filter class (MinDistMove, MaxTime for ex)
		int i = 0;
		while (i < _point.Count) 
		{
			// check individual line
			if (cullFilter.LineNotNeeded (_point[i]))
			{
				_point.RemoveAt (i);
			}
			// compare line that follows to current line (check that there is a next line)
			else if (i < _point.Count - 1)
			{
				if (cullFilter.LineNotNeeded (_point[i], _point[i + 1]))
				{
					_point.RemoveAt (i + 1);
				}
				// if no problem: proceed to next line
				else
				{
					i++;
				}
			}
			// if there is no next line, finish the while loop by checking the last line
			else 
			{
				i++;
			}
		}
	}
	
	// Create the particle in the scene view: gameobject, mesh filter and mesh renderer
	public void Create ()
	{
		if (objects.Count != 0) 
		{
			Destroy ();
			Debug.LogError ("Create () method called when particle already made in CurrentParticle.cs"); 
		}
		
		// Mesh constructing tools, used in the functions below
		Vector3[] posEnergies = new Vector3[Point.Count];
		float[] energies = new float[Point.Count];
		// Add length of charge list parameter
		int countCharge = (Charge != null) ? Charge.Count : 0;
		// create the charge construction tools
		Vector3[] posCharges = new Vector3[countCharge];
		float[] charges = new float[countCharge];;
		
		// fill these mesh constructing tools
		for(int i = 0; i < Point.Count; i++)
		{
			posEnergies[i] = Point[i].Position;
			energies[i] = Point[i].Energy;
		}
		
		// Do the same with charge tools if there is any Charge data
		if (Charge != null)
		{
			posCharges = new Vector3[Charge.Count];
			charges = new float[Charge.Count];
			
			for(int i = 0; i < Charge.Count; i++)
			{
				posCharges[i] = Charge[i].Position;
				charges[i] = Charge[i].Charge;
			}
		}
		
		// Place to add objects!
		//
		// these lines (plus declaration) are for nice view in hierarchy
		container = new GameObject ();
		container.name = Name;
		container.transform.parent = CurrentEvent.Instance.ParticleContainer.transform;
		
		// create objects used to view particle
		// the track and points meshes can be too large to fit in a single mesh: 
		// if it is the case, make several game objects
		for (int i = 0; i < MeshCreation.NumberOfMeshes (Mathf.Max (posEnergies.Length, countCharge)); i++)
		{
			string tag;
			tag = (i == 0) ? "" : " " + (i + 1).ToString ();	
			
			int begin = i * MeshCreation.maxAmountPoints;
			int length;
			Vector3[] newPos;
			float[] newScale;
			
			if (posEnergies.Length > begin)
			{
				length = Mathf.Min (MeshCreation.maxAmountPoints, posEnergies.Length - i * MeshCreation.maxAmountPoints);
				newPos = posEnergies.SubArray (begin, length);
				newScale = energies.SubArray (begin, length);
				
				objects.Add (new GameObjectComponents ("track" + tag, ParticleObjects.track, 
						MeshCreation.CreateMesh (newPos), Type.MaterialTemp, container));
				objects.Add (new GameObjectComponents ("points" + tag, ParticleObjects.points, 
				        MeshCreation.CreateMesh (ParticleObjects.points, newPos, newScale), Type.MaterialTemp, container));
			}
			if (Charge != null && countCharge > begin)
			{
				length = Mathf.Min (MeshCreation.maxAmountPoints, posCharges.Length - i * MeshCreation.maxAmountPoints);
				newPos = posCharges.SubArray (begin, length);
				newScale = charges.SubArray (begin, length);
				
				objects.Add (new GameObjectComponents ("charge" + tag, ParticleObjects.charge, 
						MeshCreation.CreateMesh (ParticleObjects.charge, newPos, newScale), Type.MaterialCharge, container));
			}
		}
		
		// the animation mesh only has one point, so no extras in mesh. Set a variable name and add to objects List
		animation = new GameObjectComponents ("animation", ParticleObjects.animation, 
				MeshCreation.CreateMesh (ParticleObjects.animation), Type.MaterialTemp, container);
		objects.Add (animation);
	} 
	
	// methods to access the gameobjects contained in "objects"
	public void Show (Filter filter)
	{
		foreach (GameObjectComponents obj in objects) 
		{
			if (filter.ViewMode [(int) obj.type])
			{
				obj.Show ();
			}
			else 
			{
				obj.Hide ();
			}
		}
	}
	public void Hide ()
	{
		foreach (GameObjectComponents obj in objects) 
		{
			obj.Hide ();
		}
	}
	public void Show (ParticleObjects type)
	{
		foreach (GameObjectComponents obj in objects) 
		{
			if (obj.type == type)
			{
				obj.Show ();
			}
		}
	}
	public void Hide (ParticleObjects type)
	{
		foreach (GameObjectComponents obj in objects) 
		{
			if (obj.type == type)
			{
				obj.Hide ();
			}
		}
	}
	public void Destroy ()
	{
		foreach (GameObjectComponents obj in objects) 
		{
			obj.Destroy ();
		}
		UnityEngine.Object.Destroy (container);
		objects.Clear ();
	}
	
	// Coroutine that animates the particle
	public IEnumerator AnimateParticle ()
	{
		// initializing: either the particle is at time zero: show it and move to next line
		// if not so wait until given time (spawn time of the particle) then show and move to next line
		// For this to work do not forget to set animInitialized to false during stop but not pause
		positionNum = 0;
		state = (Point[0].TimeStamp == 0f ? AnimParticle.Starting : AnimParticle.Waiting);
		
		// iterate through the whole list of points (for which a position "sphere" shall be shown)
		while (positionNum < Point.Count)
		{
			switch (state)
			{
				case AnimParticle.Waiting:
				
					state = AnimParticle.Starting;
					break;
				
				case AnimParticle.Starting:
				
					animation.Position = Point[positionNum].Position;
					animation.Scale = Point[positionNum].Energy;
					animation.Show ();
					positionNum ++;
					state = AnimParticle.Playing;
					break;
					
				// if already initialized, proceed to next position every iteration	
				case AnimParticle.Playing:
				
					animation.Position = Point[positionNum].Position;
					animation.Scale = Point[positionNum].Energy;
					positionNum ++;
					break;
				
				// the animation is paused: remain here until it is unpaused
				case AnimParticle.Paused:
				
					while (state == AnimParticle.Paused)
					{
						yield return null;
					}
					break;
				
				// 	the animation is stopped. Escape the coroutine.
				case AnimParticle.Done:
				
					animation.Hide ();
					yield break;	
			}
			
			// skip following positions if the time is going by too fast 
			// (if they should have already appeared, meaning timeLeft is negative)
			// and check that we are not skipping the end of the list
			while (positionNum < Point.Count && timeLeft < 0)
			{
				positionNum ++;
			}
			// wait until the timeStamp of the next position minus the current time
			if (positionNum < Point.Count)
			{					
				yield return new WaitForSeconds (timeLeft);		
			}
			// if we reached the last line (positionNum is out of bounds), 
			// wait for the same amount of time as last position shift before disappearing (first check there was a last time)
			else if (positionNum > 1)
			{
				yield return new WaitForSeconds ((Point[positionNum - 1].TimeStamp - Point[positionNum - 2].TimeStamp) 
				                                 / CurrentEvent.Instance.Speed);
			}
		}
		
		// change state, announce ending of animation (picked up by currentEvent)
		// and make particle disappear when Coroutine finishes
		state = AnimParticle.Done;
		EventRelay.RelayEvent (EventRelay.EventType.ParticleAnimFinish, this);
		Show (ParticleObjects.charge);
		animation.Hide ();
	}
	
	// class with the basics for a procedurally built object manipulation
	// contains all the subparts of the particle visualization: static display of positions, tracks, animation
	private class GameObjectComponents
	{
		private GameObject gobj = new GameObject ();
		private MeshFilter meshfil;
		private MeshRenderer meshrend;
		
		public ParticleObjects type;
		
		// fetch the transform of the game object
		public Vector3 Position 
		{
			get { return gobj.transform.position; }
			set { gobj.transform.position = value; }
		}
		
		// return scale as specified in Apppearance.cs (to keep consistent with the static display)
		public float Scale
		{
			get { return gobj.transform.localScale.x; }
			set { gobj.transform.localScale = Appearance.ReScale (type, value) * Vector3.one; }
		}
		
		// Constructor: sets name, mesh, material, container in hierarchy and hides for the moment
		public GameObjectComponents (string name, ParticleObjects objectType, Mesh mesh, Material material, GameObject container)
		{
			// set the name, container in hierarchy, type of the gameObject
			gobj.name = name;
			gobj.transform.parent = container.transform;
			type = objectType;
			
			// add, fill the meshfilter, add mesh renderer, set material and hide by default
			meshfil = gobj.AddComponent<MeshFilter> ();
			meshfil.mesh = mesh;
			meshrend = gobj.AddComponent<MeshRenderer> ();
			meshrend.enabled = false;
			meshrend.material = material;
		}
		
		// the methods to hide, show and destroy the gameobject
		public void Hide ()
		{
			meshrend.enabled = false;
		}
		public void Show ()
		{
			meshrend.enabled  = true;
		}
		public void Destroy ()
		{
			UnityEngine.Object.Destroy (gobj);
		}
	}
}