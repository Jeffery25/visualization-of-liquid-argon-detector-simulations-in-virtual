﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Particle
{
	/***********************************************
	 * 
	 * store in this class data specific to 
	 * one particle
	 * 
	 **********************************************/

	// Data concerning the particle
	protected List<DataPointTemp> _point;
	protected List<DataChargeTemp> _charge;
	protected ParticleTypeTemp _type;
	protected string _name;

	public List<DataPointTemp> Point
	{
		get { return _point; }
	}
	public List<DataChargeTemp> Charge
	{
		get { return _charge; }
	}
	public ParticleTypeTemp Type
	{
		get { return _type; }
	}
	public string Name
	{
		get { return _name; }
	}

	// Constructors:
	// empty constructor to allow the child class Current Particle to call an empty class
	public Particle () {}
	// Values inputed have just positions
	public Particle (List<DataPointTemp> dataPoints, int monteCarloNumber)
	{
		// create a copy, not just reference to the same list
		_point = new List<DataPointTemp> (dataPoints);
		SetTypeAndName (monteCarloNumber);
	}
	// Values inputed have positions and charge
	public Particle (List<DataPointTemp> dataPoints, List<DataChargeTemp> dataCharge, int monteCarloNumber) 
	{
		_point = new List<DataPointTemp> (dataPoints);
		_charge = new List<DataChargeTemp> (dataCharge);
		SetTypeAndName (monteCarloNumber);
	}
	
	// returns CurrentParticle class which is a copy of the current particle
	// but which deletes unwanted info (why the class is cloned) and can make meshes
	public CurrentParticle Make (Filter cullFilter)
	{
		return new CurrentParticle ((Particle) this.MemberwiseClone (), cullFilter);
	}
	
	private void SetTypeAndName (int monteCarloNumber)
	{
		// if the particle is a nucleus, parse the nucleus code
		// Parse nucleus code, and find in dictionary
		if (Mathf.FloorToInt (monteCarloNumber / Mathf.Pow (10, 8)) == 10) 
		{
			NucleusTemp nucleus = ParseNuclearCode (monteCarloNumber);
			if (ParticleTypeDict.NucleusDict.TryGetValue (nucleus.charge, out _type)) 
			{
				// add isotope description in name
				_name = _type.Name + " " + nucleus.baryonNum.ToString ();
			} 
			else 
			{
				_type = new ParticleTypeTemp ("Unknown Nucleus", nucleus.charge.ToString (), 
				                              ParticleFamily.Nucleus, Appearance.ColorDict [ParticleFamily.Nucleus]);
				Debug.LogError ("The nucleus code " + monteCarloNumber + " has not been recognized.");
			}
		}
		// else look up particle in dictionary, and if not found create unknown particle
		// look up for antiparticle, then particle in general with absolute value (for special antiparticles)
		else if (!ParticleTypeDict.AntiparticleDict.TryGetValue (monteCarloNumber, out _type) &&
		         !ParticleTypeDict.ParticleDict.TryGetValue (Mathf.Abs (monteCarloNumber), out _type)) 
		{
			_type = new ParticleTypeTemp ("Unknown Particle", monteCarloNumber.ToString (), 
			                              ParticleFamily.Unknown, Appearance.ColorDict [ParticleFamily.Unknown]);
			Debug.LogError ("The Monte Carlo number " + monteCarloNumber + " has not been recognized.");
		} 
		
		// name variable
		_name = _type.Name + " (" + _type.Symbol + ")";
		
		// if negative monte carlo, but type of the regular particle: make antiparticle 
		if (monteCarloNumber < 0 && _type.Antiparticle == false) 
		{
			_type.Antiparticle = true;
			_name = "anti" + _type.Name;
		}
	}
	
	// make NucleusTemp variable from nucleus code
	private NucleusTemp ParseNuclearCode (int nucleusCode)
	{
		NucleusTemp nucleus = new NucleusTemp
			(Mathf.FloorToInt( nucleusCode / 10 % Mathf.Pow (10, 3)),
			 Mathf.FloorToInt( nucleusCode / Mathf.Pow (10, 4) % Mathf.Pow (10, 3)));
		return nucleus;
	}
	
	// Template for characteristics of a nucleus
	private class NucleusTemp
	{
		public int baryonNum = 0;
		public int charge = 0;
		
		public NucleusTemp (int _baryonNum, int _charge)
		{
			baryonNum = _baryonNum;
			charge = _charge;
		}
	}
}