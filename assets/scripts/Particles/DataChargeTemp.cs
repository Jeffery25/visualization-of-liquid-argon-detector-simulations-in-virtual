﻿using UnityEngine;
using System.Collections;

public class DataChargeTemp 
{
	/***********************************************
	 * 
	 * Template for one charge line of data from
	 * Larsoft output (in "*.charge.dat" file)
	 * 
	 **********************************************/
	
	// Positions in space, energy at each given time
	// change to double if more precision needed
	private Vector3 _position;
	private float _charge;
	
	// Properties
	public Vector3 Position
	{
		get { return _position; }
	}
	
	public float Charge
	{
		get { return _charge; }
	}
	
	// Constructor
	public DataChargeTemp (float x, float y, float z, 
	                      float charge)
	{
		_position = new Vector3 (x, y, z);
		_charge = charge;
	}
	
}
