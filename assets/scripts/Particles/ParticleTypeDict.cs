﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ParticleFamily
{
	/***********************************************
	 * 
	 * Enter the different types of "families for the 
	 * particles. Distinctions given by the headers in 
	 * the Monte Carlo classification.
	 * 
	 **********************************************/

	// Debug default value
	Unknown,
	// Atom Nucleus 
	Nucleus,
	// Needed families from Monte Carlo numbering scheme
	Lepton,
	GaugeBoson,
	LightMeson,
	StrangeMeson,
	LightBaryon,
	StrangeBaryon,
	// when adding, do not forget to define a corresponding color in Appearance.cs
}

public static class ParticleTypeDict 
{
	/***********************************************
	 * 
	 * Encloses dictionaries with information on 
	 * different particle types. Most importantly connects
	 * a given Monte Carlo number to a particle. Add an 
	 * entry if a particle is not recognized.
	 * 
	 **********************************************/

	// Dictionary for different Particle types
	public static readonly Dictionary<int, ParticleTypeTemp> ParticleDict
		= new Dictionary<int, ParticleTypeTemp> ()
	{
		// Leptons : these all have different colors
		{ 11, new ParticleTypeTemp ("electron", "e\u207B", ParticleFamily.Lepton, Appearance.Col.electron) },
		{ 12, new ParticleTypeTemp ("electron neutrino", "\u03BD\u2091", ParticleFamily.Lepton, Appearance.Col.neutrinoEl) },
		{ 13, new ParticleTypeTemp ("muon", "\u03BC\u207B", ParticleFamily.Lepton, Appearance.Col.muon) },
		{ 14, new ParticleTypeTemp ("muon neutrino", "\u03BD\u03BC", ParticleFamily.Lepton, Appearance.Col.neutrinoMu) },
		{ 15, new ParticleTypeTemp ("tau", "\u03C4", ParticleFamily.Lepton, Appearance.Col.tau) },
		{ 16, new ParticleTypeTemp ("tau neutrino", "\u03BD\u03C4", ParticleFamily.Lepton, Appearance.Col.neutrinoTa) },

		// Gauge Bosons
		{ 21, new ParticleTypeTemp ("gluon", "g", ParticleFamily.GaugeBoson) },
		{ 22, new ParticleTypeTemp ("photon", "\u03B3", ParticleFamily.GaugeBoson) },
		{ 23, new ParticleTypeTemp ("Z Boson", "Z\u2070", ParticleFamily.GaugeBoson) },
		{ 24, new ParticleTypeTemp ("W Boson", "W\u207A", ParticleFamily.GaugeBoson) },

		// Light Mesons
		{ 111, new ParticleTypeTemp ("pion", "\u03C0\u2070", ParticleFamily.LightMeson) },
		{ 211, new ParticleTypeTemp ("pion", "\u03C0\u207A", ParticleFamily.LightMeson) },
		{ 221, new ParticleTypeTemp ("eta", "\u03B7", ParticleFamily.LightMeson) },

		// Strange Mesons
		{ 311, new ParticleTypeTemp ("kaon", "K\u2070", ParticleFamily.StrangeMeson) },
		{ 321, new ParticleTypeTemp ("kaon", "K\u207A", ParticleFamily.StrangeMeson) },

		// Light Baryons
		{ 2212, new ParticleTypeTemp ("proton", "p", ParticleFamily.LightBaryon, Appearance.Col.proton) },
		{ 2112, new ParticleTypeTemp ("neutron", "n", ParticleFamily.LightBaryon, Appearance.Col.neutron) },
		
		// Strange Baryons
		{ 3122, new ParticleTypeTemp ("Lambda", "\u039B", ParticleFamily.StrangeBaryon) },
		{ 3222, new ParticleTypeTemp ("Sigma plus", "\u03A3\u207A", ParticleFamily.StrangeBaryon) },
		{ 3212, new ParticleTypeTemp ("Sigma zero", "\u03A3\u2070", ParticleFamily.StrangeBaryon) },
		{ 3112, new ParticleTypeTemp ("Sigma minus", "\u03A3\u207B", ParticleFamily.StrangeBaryon) },
	};
	
	/***********************************************
	 * 
	 * Dictionary for anti particles
	 * if not included here, the antiparticles will be copies of the original 
	 * particle but with "anti" in front of their name. 
	 * If the antiparticle has more characteristics then that, add it here.
	 * 
	 **********************************************/

	public static readonly Dictionary<int, ParticleTypeTemp> AntiparticleDict
		= new Dictionary<int, ParticleTypeTemp> ()
	{
		{ -11, new ParticleTypeTemp ("positron", "e\u207A", ParticleFamily.Lepton, Appearance.Col.electron, true) },
	};
	
	/***********************************************
	 * 
	 * Dictionary for different Nucluei that can be encountered
	 * The full periodic table is included.
	 * 
	 **********************************************/

	public static readonly Dictionary<int, ParticleTypeTemp> NucleusDict
		= new Dictionary<int, ParticleTypeTemp> ()
	{
		{ 1,   new ParticleTypeTemp ("Hydrogen",     "H",   ParticleFamily.Nucleus ) },
		{ 2,   new ParticleTypeTemp ("Helium",       "He",  ParticleFamily.Nucleus ) },
		{ 3,   new ParticleTypeTemp ("Lithium",      "Li",  ParticleFamily.Nucleus ) },
		{ 4,   new ParticleTypeTemp ("Beryllium",    "Be",  ParticleFamily.Nucleus ) },
		{ 5,   new ParticleTypeTemp ("Boron",        "B",   ParticleFamily.Nucleus ) },
		{ 6,   new ParticleTypeTemp ("Carbon",       "C",   ParticleFamily.Nucleus ) },
		{ 7,   new ParticleTypeTemp ("Nitrogen",     "N",   ParticleFamily.Nucleus ) },
		{ 8,   new ParticleTypeTemp ("Oxygen",       "O",   ParticleFamily.Nucleus ) },
		{ 9,   new ParticleTypeTemp ("Fluorine",     "F",   ParticleFamily.Nucleus ) },
		{ 10,  new ParticleTypeTemp ("Neon",         "Ne",  ParticleFamily.Nucleus ) },
		{ 11,  new ParticleTypeTemp ("Sodium",       "Na",  ParticleFamily.Nucleus ) },
		{ 12,  new ParticleTypeTemp ("Magnesium",    "Mg",  ParticleFamily.Nucleus ) },
		{ 13,  new ParticleTypeTemp ("Aluminum",     "Al",  ParticleFamily.Nucleus ) },
		{ 14,  new ParticleTypeTemp ("Silicon",      "Si",  ParticleFamily.Nucleus ) },
		{ 15,  new ParticleTypeTemp ("Phosphorus",   "P",   ParticleFamily.Nucleus ) },
		{ 16,  new ParticleTypeTemp ("Sulphur",      "S",   ParticleFamily.Nucleus ) },
		{ 17,  new ParticleTypeTemp ("Chlorine",     "Cl",  ParticleFamily.Nucleus ) },
		{ 18,  new ParticleTypeTemp ("Argon",        "Ar",  ParticleFamily.Nucleus ) },
		{ 19,  new ParticleTypeTemp ("Potassium",    "K",   ParticleFamily.Nucleus ) },
		{ 20,  new ParticleTypeTemp ("Calcium",      "Ca",  ParticleFamily.Nucleus ) },
		{ 21,  new ParticleTypeTemp ("Scandium",     "Sc",  ParticleFamily.Nucleus ) },
		{ 22,  new ParticleTypeTemp ("Titanium",     "Ti",  ParticleFamily.Nucleus ) },
		{ 23,  new ParticleTypeTemp ("Vanadium",     "V",   ParticleFamily.Nucleus ) },
		{ 24,  new ParticleTypeTemp ("Chromium",     "Cr",  ParticleFamily.Nucleus ) },
		{ 25,  new ParticleTypeTemp ("Manganese",    "Mn",  ParticleFamily.Nucleus ) },
		{ 26,  new ParticleTypeTemp ("Iron",         "Fe",  ParticleFamily.Nucleus ) },
		{ 27,  new ParticleTypeTemp ("Cobalt",       "Co",  ParticleFamily.Nucleus ) },
		{ 28,  new ParticleTypeTemp ("Nickel",       "Ni",  ParticleFamily.Nucleus ) },
		{ 29,  new ParticleTypeTemp ("Copper",       "Cu",  ParticleFamily.Nucleus ) },
		{ 30,  new ParticleTypeTemp ("Zinc",         "Zn",  ParticleFamily.Nucleus ) },
		{ 31,  new ParticleTypeTemp ("Gallium",      "Ga",  ParticleFamily.Nucleus ) },
		{ 32,  new ParticleTypeTemp ("Germanium",    "Ge",  ParticleFamily.Nucleus ) },
		{ 33,  new ParticleTypeTemp ("Arsenic",      "As",  ParticleFamily.Nucleus ) },
		{ 34,  new ParticleTypeTemp ("Selenium",     "Se",  ParticleFamily.Nucleus ) },
		{ 35,  new ParticleTypeTemp ("Bromine",      "Br",  ParticleFamily.Nucleus ) },
		{ 36,  new ParticleTypeTemp ("Krypton",      "Kr",  ParticleFamily.Nucleus ) },
		{ 37,  new ParticleTypeTemp ("Rubidium",     "Rb",  ParticleFamily.Nucleus ) },
		{ 38,  new ParticleTypeTemp ("Strontium",    "Sr",  ParticleFamily.Nucleus ) },
		{ 39,  new ParticleTypeTemp ("Yttrium",      "Y",   ParticleFamily.Nucleus ) },
		{ 40,  new ParticleTypeTemp ("Zirconium",    "Zr",  ParticleFamily.Nucleus ) },
		{ 41,  new ParticleTypeTemp ("Niobium",      "Nb",  ParticleFamily.Nucleus ) },
		{ 42,  new ParticleTypeTemp ("Molybdenum",   "Mo",  ParticleFamily.Nucleus ) },
		{ 43,  new ParticleTypeTemp ("Technetium",   "Tc",  ParticleFamily.Nucleus ) },
		{ 44,  new ParticleTypeTemp ("Ruthenium",    "Ru",  ParticleFamily.Nucleus ) },
		{ 45,  new ParticleTypeTemp ("Rhodium",      "Rh",  ParticleFamily.Nucleus ) },
		{ 46,  new ParticleTypeTemp ("Palladium",    "Pd",  ParticleFamily.Nucleus ) },
		{ 47,  new ParticleTypeTemp ("Silver",       "Ag",  ParticleFamily.Nucleus ) },
		{ 48,  new ParticleTypeTemp ("Cadmium",      "Cd",  ParticleFamily.Nucleus ) },
		{ 49,  new ParticleTypeTemp ("Indium",       "In",  ParticleFamily.Nucleus ) },
		{ 50,  new ParticleTypeTemp ("Tin",          "Sn",  ParticleFamily.Nucleus ) },
		{ 51,  new ParticleTypeTemp ("Antimony",     "Sb",  ParticleFamily.Nucleus ) },
		{ 52,  new ParticleTypeTemp ("Tellurium",    "Te",  ParticleFamily.Nucleus ) },
		{ 53,  new ParticleTypeTemp ("Iodine",       "I",   ParticleFamily.Nucleus ) },
		{ 54,  new ParticleTypeTemp ("Xenon",        "Xe",  ParticleFamily.Nucleus ) },
		{ 55,  new ParticleTypeTemp ("Cesium",       "Cs",  ParticleFamily.Nucleus ) },
		{ 56,  new ParticleTypeTemp ("Barium",       "Ba",  ParticleFamily.Nucleus ) },
		{ 57,  new ParticleTypeTemp ("Lanthanum",    "La",  ParticleFamily.Nucleus ) },
		{ 58,  new ParticleTypeTemp ("Cerium",       "Ce",  ParticleFamily.Nucleus ) },
		{ 59,  new ParticleTypeTemp ("Praseodymium", "Pr",  ParticleFamily.Nucleus ) },
		{ 60,  new ParticleTypeTemp ("Neodymium",    "Nd",  ParticleFamily.Nucleus ) },
		{ 61,  new ParticleTypeTemp ("Promethium",   "Pm",  ParticleFamily.Nucleus ) },
		{ 62,  new ParticleTypeTemp ("Samarium",     "Sm",  ParticleFamily.Nucleus ) },
		{ 63,  new ParticleTypeTemp ("Europium",     "Eu",  ParticleFamily.Nucleus ) },
		{ 64,  new ParticleTypeTemp ("Gadolinium",   "Gd",  ParticleFamily.Nucleus ) },
		{ 65,  new ParticleTypeTemp ("Terbium",      "Tb",  ParticleFamily.Nucleus ) },
		{ 66,  new ParticleTypeTemp ("Dysprosium",   "Dy",  ParticleFamily.Nucleus ) },
		{ 67,  new ParticleTypeTemp ("Holmium",      "Ho",  ParticleFamily.Nucleus ) },
		{ 68,  new ParticleTypeTemp ("Erbium",       "Er",  ParticleFamily.Nucleus ) },
		{ 69,  new ParticleTypeTemp ("Thulium",      "Tm",  ParticleFamily.Nucleus ) },
		{ 70,  new ParticleTypeTemp ("Ytterbium",    "Yb",  ParticleFamily.Nucleus ) },
		{ 71,  new ParticleTypeTemp ("Lutetium",     "Lu",  ParticleFamily.Nucleus ) },
		{ 72,  new ParticleTypeTemp ("Hafnium",      "Hf",  ParticleFamily.Nucleus ) },
		{ 73,  new ParticleTypeTemp ("Tantalum",     "Ta",  ParticleFamily.Nucleus ) },
		{ 74,  new ParticleTypeTemp ("Tungsten",     "W",   ParticleFamily.Nucleus ) },
		{ 75,  new ParticleTypeTemp ("Rhenium",      "Re",  ParticleFamily.Nucleus ) },
		{ 76,  new ParticleTypeTemp ("Osmium",       "Os",  ParticleFamily.Nucleus ) },
		{ 77,  new ParticleTypeTemp ("Iridium",      "Ir",  ParticleFamily.Nucleus ) },
		{ 78,  new ParticleTypeTemp ("Platinum",     "Pt",  ParticleFamily.Nucleus ) },
		{ 79,  new ParticleTypeTemp ("Gold",         "Au",  ParticleFamily.Nucleus ) },
		{ 80,  new ParticleTypeTemp ("Mercury",      "Hg",  ParticleFamily.Nucleus ) },
		{ 81,  new ParticleTypeTemp ("Thallium",     "Tl",  ParticleFamily.Nucleus ) },
		{ 82,  new ParticleTypeTemp ("Lead",         "Pb",  ParticleFamily.Nucleus ) },
		{ 83,  new ParticleTypeTemp ("Bismuth",      "Bi",  ParticleFamily.Nucleus ) },
		{ 84,  new ParticleTypeTemp ("Polonium",     "Po",  ParticleFamily.Nucleus ) },
		{ 85,  new ParticleTypeTemp ("Astatine",     "At",  ParticleFamily.Nucleus ) },
		{ 86,  new ParticleTypeTemp ("Radon",        "Rn",  ParticleFamily.Nucleus ) },
		{ 87,  new ParticleTypeTemp ("Francium",     "Fr",  ParticleFamily.Nucleus ) },
		{ 88,  new ParticleTypeTemp ("Radium",       "Ra",  ParticleFamily.Nucleus ) },
		{ 89,  new ParticleTypeTemp ("Actinium",     "Ac",  ParticleFamily.Nucleus ) },
		{ 90,  new ParticleTypeTemp ("Thorium",      "Th",  ParticleFamily.Nucleus ) },
		{ 91,  new ParticleTypeTemp ("Protactinium", "Pa",  ParticleFamily.Nucleus ) },
		{ 92,  new ParticleTypeTemp ("Uranium",      "U",   ParticleFamily.Nucleus ) },
		{ 93,  new ParticleTypeTemp ("Neptunium",    "Np",  ParticleFamily.Nucleus ) },
		{ 94,  new ParticleTypeTemp ("Plutonium",    "Pu",  ParticleFamily.Nucleus ) },
		{ 95,  new ParticleTypeTemp ("Americium",    "Am",  ParticleFamily.Nucleus ) },
		{ 96,  new ParticleTypeTemp ("Curium",       "Cm",  ParticleFamily.Nucleus ) },
		{ 97,  new ParticleTypeTemp ("Berkelium",    "Bk",  ParticleFamily.Nucleus ) },
		{ 98,  new ParticleTypeTemp ("Californium",  "Cf",  ParticleFamily.Nucleus ) },
		{ 99,  new ParticleTypeTemp ("Einsteinium",  "Es",  ParticleFamily.Nucleus ) },
		{ 100, new ParticleTypeTemp ("Fermium",      "Fm",  ParticleFamily.Nucleus ) },
		{ 101, new ParticleTypeTemp ("Mendelevium",  "Md",  ParticleFamily.Nucleus ) },
		{ 102, new ParticleTypeTemp ("Nobelium",     "No",  ParticleFamily.Nucleus ) },
		{ 103, new ParticleTypeTemp ("Lawrencium",   "Lr",  ParticleFamily.Nucleus ) },
		{ 104, new ParticleTypeTemp ("Rutherfordium","Rf",  ParticleFamily.Nucleus ) },
		{ 105, new ParticleTypeTemp ("Dubnium",      "Db",  ParticleFamily.Nucleus ) },
		{ 106, new ParticleTypeTemp ("Seaborgium",   "Sg",  ParticleFamily.Nucleus ) },
		{ 107, new ParticleTypeTemp ("Bohrium",      "Bh",  ParticleFamily.Nucleus ) },
		{ 108, new ParticleTypeTemp ("Hassium",      "Hs",  ParticleFamily.Nucleus ) },
		{ 109, new ParticleTypeTemp ("Meitnerium",   "Mt",  ParticleFamily.Nucleus ) },
		{ 110, new ParticleTypeTemp ("Darmstadtium", "Ds",  ParticleFamily.Nucleus ) },
		{ 112, new ParticleTypeTemp ("Ununbium",     "Uub", ParticleFamily.Nucleus ) },
		{ 114, new ParticleTypeTemp ("Ununquadium",  "Uuq", ParticleFamily.Nucleus ) },
		{ 116, new ParticleTypeTemp ("Ununhexium",   "Uuh", ParticleFamily.Nucleus ) },
	};
	
}

