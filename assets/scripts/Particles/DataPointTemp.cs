﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DataPointTemp
{
	/***********************************************
	 * 
	 * Template for one data point from 
	 * Larsoft output (in "*.particles.dat" file)
	 * 
	 **********************************************/
	
	// Positions in space, energy at each given time
	// change to double if more precision needed
	private Vector3 _position;
	private float _energy, _timeStamp;
	
	// Properties
	public Vector3 Position
	{
		get { return _position; }
	}
	
	public float Energy 
	{
		get { return _energy; }
	}
	
	public float TimeStamp 
	{
		get { return _timeStamp; }
	}
	
	// Constructor
	public DataPointTemp (float x, float y, float z, 
	                      float energy, float timeStamp)
	{
		_position = new Vector3 (x, y, z);
		_energy = energy;
		_timeStamp = timeStamp;
	}
}