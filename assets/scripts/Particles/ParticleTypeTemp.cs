﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleTypeTemp
{
	/***********************************************
	 * 
	 * Template for information on different 
	 * particle types. Each recognized particle 
	 * references one.
	 * 
	 **********************************************/
	
	// The info we want for each particle
	// Unknown value for debugging
	// can add more info
	private string _name;
	private string _symbol;
	private bool _antiparticle; // changed in Particle.cs or in constructor if not false
	private ParticleFamily _family = ParticleFamily.Unknown;
	
	// Subjective visual characteristics 
	private Material _materialTemp = new Material (Appearance.particleMaterial); 
	private Material _materialCharge = new Material (Appearance.chargeMaterial); 
	
	// Properties
	public string Name
	{
		get { return _name; }
	}
	public string Symbol
	{
		get { return _symbol; }
	}
	public ParticleFamily Family
	{
		get { return _family; }
	}
	public bool Antiparticle
	{
		get { return _antiparticle; }
		set { _antiparticle = value; }
	}
	public Material MaterialTemp
	{
		get { return _materialTemp; }
	}
	public Material MaterialCharge
	{
		get { return _materialCharge; }
	}
	
	// Constructors
	// For important particles where the visual characteristics are unique
	public ParticleTypeTemp (string name, string symbol, 
	                         ParticleFamily family, Color coloring)
	{
		_name = name;
		_symbol = symbol;
		_family = family;
		_materialTemp.color = coloring; 
		_materialCharge.color = Appearance.Col.charge;
	}
	// For antiparticles in the AntiparticleDict dictionary (make the bool true)
	public ParticleTypeTemp (string name, string symbol, 
	                         ParticleFamily family, Color coloring, bool antiparticle)
	{
		_name = name;
		_symbol = symbol;
		_family = family;
		_antiparticle = antiparticle;
		_materialTemp.color = coloring; 
		_materialCharge.color = Appearance.Col.charge;
	}
	// For more generic particles where the visual characteristics depend on the family 
	public ParticleTypeTemp (string name, string symbol, 
	                         ParticleFamily family)
	{
		// temporary dictionary retrieval color variable
		Color coloring;
		
		_name = name;
		_symbol = symbol;
		_family = family;
		
		// look for relevant color in dictionary
		if (Appearance.ColorDict.TryGetValue (family, out coloring)) 
		{
			_materialTemp.color = coloring; 
			_materialCharge.color = Appearance.Col.charge;
		}
		else
		{
			Debug.LogError("The family " + Family + " has no color defined.");
		}
	}
}