﻿using UnityEngine;
using System.Collections;

	/***********************************************
	 * 
	 * Here are defined the different interfaces 
	 * used in the program
	 * 
	 **********************************************/

public interface IEventHandler
{
	string HandleEvent (EventRelay.EventType messageType, System.Object sender);
}