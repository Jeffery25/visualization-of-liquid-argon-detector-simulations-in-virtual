﻿using UnityEngine;
using System.Collections;

public class EventRelay
{
	/***********************************************
	 * 
	 * Event Relayer, taken from Lynda tutorial.
	 * All events in program are liste in the EventType
	 * enum, and then relayed using this class. See 
	 * instructions in comments below.
	 * 
	 **********************************************/

	// Declare delegate and event
	public delegate string EventAction(EventType type, System.Object sender);
	public static event EventAction OnEventAction;

	// All the different messages here
	public enum EventType 
	{
		// option in a Filter class changed
		FilterChange,
		// a particle animation sends this message when finished
		ParticleAnimFinish,
	}

	// call to make event
	public static string RelayEvent(EventType messageType, System.Object sender) 
	{
		if (!Manager.Instance.messagesSuspend)
		{
			return OnEventAction(messageType, sender);
		}
		else
		{
			return "Messages suspended.";
		}
	}
	

//	On Sender side:
//
//	string [receivedBy] = EventRelay.RelayEvent(
//		EventRelay.EventType.[eventName], this);
//
//
//	On Listener side:
//
//	Implement IEventHandler (for visibility)
//
//	void OnEnable() 
//  {
//		EventRelay.OnEventAction += HandleEvent;
//	}
//	void OnDisable() 
//	{
//		EventRelay.OnEventAction -= HandleEvent;
//	}
//	
//	public List<EventRelay.EventType> eventsHandled =
//		new List<EventRelay.EventType> ();
//	
//	public string HandleEvent(EventRelay.EventType messageType, System.Object sender) 
//	{
//		if (eventsHandled.Contains(messageType)) 
//		{
//			// handle event with sender
//			return this.ToString ();
//		} 
//		else 
//		{
//			// ignore event
//			return this.ToString ();
//		}
//	}

}
