using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CurrentEvent : Singleton<CurrentEvent>, IEventHandler
{
	/***********************************************
	 * 
	 * Contains the particles currently considered, 
	 * and displays them on the screen in the mode
	 * wanted (static or dynamic, lines and/or points)
	 * Used by the Manager class
	 * 
	 **********************************************/
	
	// All operations are done on the particles (class CurrentParticles)
	// avoid using the property recursively because of the logic inside
	private List<CurrentParticle> curParticles;
	private Filter cullFilter = FilterManage.Instance.Cull;
	private Filter currentFilter = FilterManage.Instance.Auto;
	
	// conatiner of the particles as gameobject
	public GameObject ParticleContainer { get; set; }
	
	// variables for animation 
	public float _speed = 5f; // if animation running, what speed (1 is 1ns/s)? public for inspector access
	private AnimationState animState = AnimationState.Disactivated; // tells in which state the animation is
	private int numParticleAnimated; // number of particles in animation, compare this value to animParticlesFinished to monitor progress
	private int animParticlesFinish = 0; // number of particles that finished animating and have disappeared
	private bool playing; // false if animation not playing
	private float startPlayTime; // when was the playing mode enabled for the simulation? absolute time 
	private float simulationTime; // the relative time in the current animation (only updated when simulation paused)
	
	private enum AnimationState
	{
		Playing, // animation playing
		Unpaused, // playing after a pause
		Paused, // pause when repress play button in play mode
		Stopped, // reaches this state when play reaches maxTime
		Disactivated, // showing the static positions
	}
	
	// Properties
	// Particles indirectly fills curParticles
	public List<Particle> Particles
	{
		set 
		{
			// remove the gameobjects in scene
			if (curParticles != null)
			{
				DisactivateAnim ();
				DestroyParticles ();
			}
			// initialize the values
			curParticles = Cull (value);
			Initialize ();
		}
	}
	// speed of animation (in nanoseconds per second)
	public float Speed
	{
		get { return _speed; }
		set 
		{
			if (_speed > 0)
			{
				_speed = value;
			}
			else
			{
				Debug.LogError ("Speed of particle animation set to a negative value");
			}
		}
	}
	// time in seconds since animation started
	public float time
	{
		get 
		{ 
			if (playing)
			{
				return Time.time - (startPlayTime - simulationTime); 
			}
			else 
			{
				return simulationTime;
			}
		}
	}
	
	// allow the handling of events and start a container for hierarchy view
	void OnEnable() 
  	{
		EventRelay.OnEventAction += HandleEvent;
		
		ParticleContainer = new GameObject ();
		ParticleContainer.name = "Particles";
	}

	void OnDisable() 
	{
		EventRelay.OnEventAction -= HandleEvent;
	}

	void Update ()
	{
		// Play/Pause and stop buttons to control animation
		if (Input.GetKeyDown(UserControls.playOrPause)) 
		{
			PlayOrPause(currentFilter);
		}
		if (Input.GetKeyDown(UserControls.stop)) 
		{
			DisactivateAnim();
		}
	}
	
	// event handling, in particular the change of a filter returns the name of this class
	public string HandleEvent (EventRelay.EventType messageType, System.Object sender) 
	{
		// when the filter changes, update the scene
		if (messageType == EventRelay.EventType.FilterChange) 
		{
			UpdateScene (currentFilter);
			return this.ToString ();
		}
		// when all particles in the animation reach the end, change from
		// play to stop and reset the scene for a new animation
		if (messageType == EventRelay.EventType.ParticleAnimFinish)
		{
			animParticlesFinish ++;
			if (animParticlesFinish == numParticleAnimated)
			{
				ResetAnimation ();
				animState = AnimationState.Stopped;
			}
			return this.ToString ();
		}
		else 
		{
			return this.ToString ();
		}
	}

	// Makes all particles create gameObjects, meshes, and then makes them appear
	private void Initialize ()
	{
		foreach (CurrentParticle particle in curParticles) 
		{
			particle.Create ();
		}
		// show the meshes using the current filter 
		UpdateScene (currentFilter);
	}
	
	// update the scene (ie when filters are changed)
	private void UpdateScene (Filter filter)
	{
		// situations in which the scene should not be updated until later
		// for instance here an animation should not be running
		if (!playing && animState != AnimationState.Paused)
		{
			// for each particle that can be shown filter out unwanted ones
			foreach (CurrentParticle particle in curParticles) 
			{
				if (filter.CanBeSeen (particle))
				{
					particle.Show (filter);
				}
				else
				{
					particle.Hide ();
				}
			}
		}
		else
		{
			Debug.Log ("Wait until end of animation for the filter change to apply.");
		}
	}

	// destroys content of relevant Particle
	private void DestroyParticles ()
	{
		if (curParticles != null) 
		{
			foreach (CurrentParticle particle in curParticles) 
			{
				particle.Destroy ();
			}
		}
	}

	// takes only the relevant data from the Particle class and fills the 
	// curParticle class which is then used in displaying
	private List<CurrentParticle> Cull (List<Particle> givenParticles)
	{
		// new list of particles to be used
		List<CurrentParticle> newParticles = new List<CurrentParticle> ();
		CurrentParticle tempPart;
		
		// check that each particle can be seen before making
		foreach (Particle particle in givenParticles) 
		{
			if (cullFilter.CanBeSeen (particle)) 
			{
				// the cull filter will remove unwanted lines:
				// if all lines are removed, do not add the particle
				tempPart = particle.Make (cullFilter);
				if (tempPart.Point.Count != 0)
				{
					newParticles.Add (tempPart);
				}
			}
		}
		return newParticles;
	}
	
	/***********************************************
	 * 
	 * Animation methods: playOrPause, DisactivateAnim
	 * (= stop button), start, stop, reset, cahange state
	 * 
	 **********************************************/
	
	private void PlayOrPause (Filter filter)
	{
		switch (animState)
		{
			case AnimationState.Disactivated:
			case AnimationState.Paused:
			case AnimationState.Stopped:
				// switch the animation state
				animState = ChangeState (animState);
				// start the timer
				startPlayTime = Time.time;
				// start the particle animation or coroutines
				StartAnimation (filter, animState);
				break;

			case AnimationState.Unpaused:
			case AnimationState.Playing:
				// switch the animation state
				animState = ChangeState (animState);
				// pause the timer
				simulationTime += Time.time - startPlayTime;
				// make the animation end or stop
				StopAnimation (animState);
				break;
		}
		// toggle playing bool
		playing = !playing;
	}
	
	private void DisactivateAnim ()
	{
		// reset the animation and set state to disactivated
		ResetAnimation ();
		animState = AnimationState.Disactivated; 
		
		// make the animation end or stop
		StopAnimation (animState);
		
		// show the static display of all points which also hides all the animation points
		// (so they don't pop up in same position when coming back to play mode)
		FilterChange.Change (currentFilter.ViewMode, (int) ParticleObjects.points, true, this);
		FilterChange.Change (currentFilter.ViewMode, (int) ParticleObjects.charge, true, this);
	}
	
	private void StartAnimation (Filter filter, AnimationState state)
	{
		if (state != AnimationState.Unpaused)
		{
			numParticleAnimated = 0;
			foreach (CurrentParticle particle in curParticles) 
			{
				// only show animations we want to see
				if (filter.CanBeSeen (particle))
				{
					numParticleAnimated ++;
					StartCoroutine (particle.AnimateParticle ());
				}
			}
		}
		// if the animation is paused simply make the coroutines resume
		else
		{
			foreach (CurrentParticle particle in curParticles) 
			{
				particle.ResetState ();
			}
		}
	}
	
	private void StopAnimation (AnimationState state)
	{
		switch (state)
		{
			case AnimationState.Disactivated:
			
				foreach (CurrentParticle particle in curParticles) 
				{
					particle.state = CurrentParticle.AnimParticle.Done;
				}
				break;
				
			case AnimationState.Paused:
			
				foreach (CurrentParticle particle in curParticles) 
				{
					particle.state = CurrentParticle.AnimParticle.Paused;
				}
				break;
		}
	}
	
	// reset the animation, used when animation stopped or disactivated
	// reset the time, set playing to false and the count of finished particles to 0
	// finally update scene
	private void ResetAnimation ()
	{
		simulationTime = 0f;
		animParticlesFinish = 0;
		playing = false;
		UpdateScene (currentFilter);
	}
	
	// change the animation state in the PlayorPause function
	private AnimationState ChangeState (AnimationState state)
	{
		switch (state)
		{
			case AnimationState.Disactivated:
			
				// hide the static display of all points and charges, but then reactivate 
				// the charges without updating the scene so that they can be shown little by little
				FilterChange.Change (currentFilter.ViewMode, (int) ParticleObjects.points, false, this);
				FilterChange.Change (currentFilter.ViewMode, (int) ParticleObjects.charge, false, this);
				FilterChange.Change (currentFilter.ViewMode, (int) ParticleObjects.charge, true, this, false);
				return AnimationState.Playing;
				
			case AnimationState.Stopped:
			
				FilterChange.Change (currentFilter.ViewMode, (int) ParticleObjects.charge, false, this);
				FilterChange.Change (currentFilter.ViewMode, (int) ParticleObjects.charge, true, this, false);
				return AnimationState.Playing;
				
			case AnimationState.Paused:
			
				return AnimationState.Unpaused;
							
			default: // this includes playing and unpaused
			
				return AnimationState.Paused;
			
		}
	}
}