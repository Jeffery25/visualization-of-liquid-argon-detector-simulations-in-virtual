﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class Event
{
	/***********************************************
	 * 
	 * Contains all of the different particles in 
	 * one event
	 * 
	 **********************************************/
	
	// instantiate a list of particles to fill
	public readonly List<Particle> particles = new List<Particle> ();
	
	// verifies if the data files types (determined by the second extension)
	// that this class is capable of reading are currently in use
	private int p = Syntax.extSecond.FindIndex(a => a == "particles");
	private int c = Syntax.extSecond.FindIndex(a => a == "charge");
	
	// Constructor
	public Event (List<string>[] eventLinesCompil, List<int>[] particleIndexes)
	{
		int types = Syntax.extSecond.Count;
		bool error = false;
		
		if (eventLinesCompil.Length != types || particleIndexes.Length != types)
		{
			error = true;
			Debug.LogError ("Event class constructor given arrays with incorrect dimensions.");
		}
		// if dimensions have no errors, create particles
		if (!error)
		{
			// variables used in the Constructor
			int chargeNum = 0;
			int monteCarloNumber;
			List<string> particleLinesPoints;
			List<string> particleLinesCharge;
			
			// add length to particleIndexes to avoid breaking following for loop
			for (int i = 0; i < types; i++)
			{
				particleIndexes[i].Add (eventLinesCompil[i].Count);
			}
			
			// initialize the charge list
			particleLinesCharge = eventLinesCompil [c].GetRange (particleIndexes [c][0], particleIndexes [c][1]);
			
			// Make Particles for each particle token in event
			for (int i = 0; i < particleIndexes[p].Count - 1; i++) 
			{
				// get lines for one particle
				particleLinesPoints = eventLinesCompil [p].GetRange (particleIndexes [p][i], 
					particleIndexes [p][i + 1] - particleIndexes [p][i]);
				
				monteCarloNumber = ParseType (particleLinesPoints[0]);
				
				if (monteCarloNumber == ParseType (particleLinesCharge[0]) && 
					StartTime (particleLinesPoints[1]) == StartTime (particleLinesCharge[1]))
				{
					// Instantiate the Particle and clear data set
					particles.Add (new Particle (ParsePoint (particleLinesPoints), ParseCharge (particleLinesCharge), monteCarloNumber));
					// update the charge lines and counter
					chargeNum ++;
					if (chargeNum < particleIndexes [c].Count - 1)
					{
						particleLinesCharge = eventLinesCompil [c].GetRange (particleIndexes [c][chargeNum], 
					    		particleIndexes [c][chargeNum + 1] - particleIndexes [c][chargeNum]);
					}
				}
				else
				{
					particles.Add (new Particle (ParsePoint (particleLinesPoints), monteCarloNumber));
				}
			}
			// verify that all particles in the charge file have been matched with a particle in particles file
			if (chargeNum + 1 != particleIndexes [c].Count)
			{
				Debug.LogError ((particleIndexes [c].Count - (chargeNum + 1)).ToString () + " out of " + (particleIndexes [c].Count).ToString () + 
						" charge data lists have not been matched with the relevant particle.");
			}
		}
	}
	
	private int ParseType (string tokenLine)
	{
		// set to minus one in case of parsing error
		int monteCarloNumber = -1;
		
		// retrieve Monte Carlo number
		try
		{
			monteCarloNumber = Int32.Parse (tokenLine.Substring 
			                                (Syntax.particleToken.Length + Syntax.splitToken.Length));
		}
		catch (FormatException)
		{
			Debug.LogError ("The line " + tokenLine[0] + " is in an incorrect format. " +
			                "Expected: '" + Syntax.particleToken + Syntax.splitToken + "int'." +
			                " The particle will not be created.");
		}
		
		return monteCarloNumber;
	}
	
	private List<DataPointTemp> ParsePoint (List<string> particleLines)
	{
		List<DataPointTemp> data = new List<DataPointTemp> ();
		string[] line;
		
		// for each line after the token line create a data point
		for (int j = 1; j < particleLines.Count; j++)
		{
			// allow for empty lines
			if (particleLines[j] != "")
			{
				// split line, check for correct number of columns and try converting to float values
				line = particleLines[j].Split ( new[] { Syntax.splitData }, StringSplitOptions.RemoveEmptyEntries);
				if (line.Length == Syntax.numColumns)
				{
					try
					{
						data.Add (new DataPointTemp (
							Single.Parse (line[Syntax.x]), 
							Single.Parse (line[Syntax.y]), 
							Single.Parse (line[Syntax.z]),
							Single.Parse (line[Syntax.energy]),
							Single.Parse (line[Syntax.timeStamp])));
					}
					catch (FormatException)
					{
						Debug.LogError("The line " + particleLines[j] + " contains invalid float values.");						               
					}
				}
				else
				{
					Debug.LogError ("The line " + particleLines[j] + " is in an incorrect format. " +
					                "Expected: " + Syntax.numColumns + " columns of data.");
				}
			}
		}
		
		return data;
	}
	
	private List<DataChargeTemp> ParseCharge (List<string> particleLines)
	{
		List<DataChargeTemp> data = new List<DataChargeTemp> ();
		string[] line;
		
		// for each line after the token line create a data point
		for (int j = 1; j < particleLines.Count; j++)
		{
			// allow for empty lines
			if (particleLines[j] != "")
			{
				// split line, check for correct number of columns and try converting to float values
				line = particleLines[j].Split ( new[] { Syntax.splitData }, StringSplitOptions.RemoveEmptyEntries);
				if (line.Length == Syntax.numColumns)
				{
					try
					{
						data.Add (new DataChargeTemp (
							Single.Parse (line[Syntax.x]), 
							Single.Parse (line[Syntax.y]), 
							Single.Parse (line[Syntax.z]),
							Single.Parse (line[Syntax.charge])));
					}
					catch (FormatException)
					{
						Debug.LogError("The line " + particleLines[j] + " contains invalid float values.");						               
					}
				}
				else
				{
					Debug.LogError ("The line " + particleLines[j] + " is in an incorrect format. " +
					                "Expected: " + Syntax.numColumns + " columns of data.");
				}
			}
		}
		
		return data;
	}
	
	private float StartTime (string firstDataLine)
	{
		float startTime = 0;
		string[] line;
		
		// allow for empty lines
		if (firstDataLine != "")
		{
			// split line, check for correct number of columns and try converting to float values
			line = firstDataLine.Split ( new[] { Syntax.splitData }, StringSplitOptions.RemoveEmptyEntries);
			if (line.Length == Syntax.numColumns)
			{
				try
				{
					startTime = Single.Parse (line[Syntax.timeStamp]);
				}
				catch (FormatException)
				{
					Debug.LogError("TimeStamp data could not be read from the line " + firstDataLine + ".");						               
				}
			}
			else
			{
				Debug.LogError ("The line " + firstDataLine + " is in an incorrect format. " +
				                "Expected: " + Syntax.numColumns + " columns of data.");
			}
		}
		else
		{
			Debug.LogError ("StartTime method in Event.cs given an empty line");
		}
		
		return startTime;
	}	
}
