﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class FilterManage : Singleton<FilterManage>
{
	/***********************************************
	 * 
	 * Initializes and contains the filters used to 
	 * show or hide particles,
	 * Also has a very prototopical User Interface.
	 * 
	 **********************************************/
	
	// the particles filtered will never be instantiated in the scene
	public Filter Cull = new Filter (0.001f, 20f);
	
	// the automatic filter applied on the given scene
	public Filter Auto = new Filter (0f, 10f, false, false, new List<string> (), new List<ParticleFamily> ());
	
	// Filters that can be applied after the inital rendering
	public List<Filter> Custom = new List<Filter> ();
	
	// User Interface
	void Update ()
	{
		if (Input.GetKeyDown(UserControls.toggleAntiparticles)) 
		{
			if (Auto.Antiparticles == null)
			{
				Auto.Antiparticles = true;
			}
			Auto.Antiparticles = !Auto.Antiparticles;
			print ("Showing antiparticles: " + Auto.Antiparticles);
		}
		if (Input.GetKeyDown(UserControls.stopToggleAntiparticles)) 
		{
			Auto.Antiparticles = null;
		}
		if (Input.GetKeyDown (UserControls.higherMaxEnergy) && Auto.MaxIniEnergy != null) 
		{
			Auto.MaxIniEnergy += 2;
			print ("Max energy: " + Auto.MaxIniEnergy + " GeV."); 
		}
		if (Input.GetKeyDown (UserControls.lowerMaxEnergy) && Auto.MaxIniEnergy != null) 
		{
			Auto.MaxIniEnergy -= 2;
			print ("Max energy: " + Auto.MaxIniEnergy + " GeV."); 
		}
		if (Input.GetKeyDown (UserControls.higherMinLifeSpan) && Auto.MinLifeSpan != null) 
		{
			Auto.MinLifeSpan ++;
			print ("Minimum LifeSpan: " + Auto.MinLifeSpan + " ns."); 
		}
		if (Input.GetKeyDown (UserControls.lowerMinLifeSpan) && Auto.MinLifeSpan != null) 
		{
			Auto.MinLifeSpan --;
			print ("Minimum LifeSpan: " + Auto.MinLifeSpan + " ns."); 
		}
		if (Input.GetKeyDown (UserControls.filterNeutrons)) 
		{
			FilterChange.Add<string> (Auto.FilteredType, "neutron", this);
			print ("Filtering neutrons");
		}
		if (Input.GetKeyDown (UserControls.filterElectrons)) 
		{
			FilterChange.Add<string> (Auto.FilteredType, "electron", this);
			FilterChange.Add<string> (Auto.FilteredType, "positron", this);
			print ("Filtering electrons and positrons");
		}
		if (Input.GetKeyDown (UserControls.filterPhotons)) 
		{
			FilterChange.Add<string> (Auto.FilteredType, "photon", this);
			print ("Filtering photons");
		}
		if (Input.GetKeyDown (UserControls.filterNuclei)) 
		{
			FilterChange.Add<ParticleFamily> (Auto.FilteredFamily, ParticleFamily.Nucleus, this);;
			print ("Filtering nuclei");
		}
		if (Input.GetKeyDown (UserControls.clear)) 
		{
			FilterChange.Clear (Auto.FilteredType, this);
			FilterChange.Clear (Auto.FilteredFamily, this);
			print ("Clear");
		}
		if (Input.GetKeyDown(UserControls.toggleCharge)) 
		{
			FilterChange.Change (Auto.ViewMode, (int) ParticleObjects.charge, !Auto.ViewMode[(int) ParticleObjects.charge], this);
			print ("Toggling charge");
		}
		if (Input.GetKeyDown(UserControls.togglePoints)) 
		{
			FilterChange.Change (Auto.ViewMode, (int) ParticleObjects.points, !Auto.ViewMode[(int) ParticleObjects.points], this);
			print ("Toggling points");
		}
	}
}
