﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class FilterChange
{
	public static void Add<T> (List<T> list, T element, System.Object caller)
	{
		list.Add (element);
		EventRelay.RelayEvent (EventRelay.EventType.FilterChange, caller);
	}
	public static void Clear<T> (List<T> list, System.Object caller)
	{
		list.Clear ();
		EventRelay.RelayEvent (EventRelay.EventType.FilterChange, caller);
	}
	public static void Change<T> (T[] array, int index, T value, System.Object caller)
	{
		Change<T> (array, index, value, caller, true);
	}
	public static void Change<T> (T[] array, int index, T value, System.Object caller, bool update)
	{
		array[index] = value;
		if (update)
		{
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, caller);
		}
	}
}