﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Filter
{
	/***********************************************
	 * 
	 * The template for any filter. These specify which 
	 * particles to show in the scene view with many different 
	 * parameters. The Cull filter also selects the data points
	 * of interest. Filters can be customizable, and are stored 
	 * in the FilterManage class. 
	 * 
	 **********************************************/

	// the ? marks nullable types: allows the filter to be disactivated
	// the following are default values

	// particle needs to respect following conditions to appear
	private float? _maxLifeSpan = null; // nanoseconds
	private float? _minLifeSpan = null; // nanoseconds
	private float? _maxIniEnergy = null; // GeV
	private float? _minIniEnergy = null; // GeV

	// time frame we observe
	private float? _startedBefore = null; // nanoseconds
	private float? _startedAfter = null; // nanoseconds
	private float? _endedBefore = null; // nanoseconds
	private float? _endedAfter = null; // nanoseconds
	
	// view particles or antiparticles
	private bool? _antiparticles = null;

	// makes the lists below filter additively or subtractively
	// made false when the lists are empty
	private bool _additiveFilterType = false;
	private bool _additiveFilterFamily = true;

	// particles we don't want to see
	private List<string> _filteredType = new List<string> ();
	private List<ParticleFamily> _filteredFamily = new List<ParticleFamily> ();

	// The variables used for line by line selection of data (in Particle class)
	// useful only for the cull filter, which eliminates unnecessary lines as well
	// (ie the particle is in the same position) 
	private float MinDistMove { get; set; } // in cm
	private float? MaxTime  { get; set; } // in ns
	
	// Different modes of visualization. For each "object" (track, position points, charge points)
	// in the particle class define a boolean deciding whether or not the object is shown
	// By default all are set to false automatically
	private bool[] _viewMode = new bool[Enum.GetNames(typeof(ParticleObjects)).Length];
	
	// Properties
	// check for inconsistencies and send update messages when values are changed
	// if two values clash, one is kept by default;
	public float? MaxLifeSpan 
	{
		get { return _maxLifeSpan; }
		set 
		{ 
			_maxLifeSpan = value;
			if (_maxLifeSpan != null && _minLifeSpan != null)
				if (_maxLifeSpan <= _minLifeSpan) 
				{
					_maxLifeSpan = null;
					Debug.LogError ("Incorrect value on maximum life span");
				}
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public float? MinLifeSpan 
	{
		get { return _minLifeSpan; }
		set 
		{ 
			_minLifeSpan = value;
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public float? MaxIniEnergy 
	{
		get { return _maxIniEnergy; }
		set 
		{ 
			_maxIniEnergy = value;
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public float? MinIniEnergy 
	{
		get { return _minIniEnergy; }
		set 
		{ 
			_minIniEnergy = value;
			if (_maxIniEnergy != null && _minIniEnergy != null)
				if (_maxIniEnergy <= _minIniEnergy) 
			{
				_minIniEnergy = null;
				Debug.LogError ("Incorrect value on minimum initial energy");
			}
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public float? StartedBefore
	{
		get { return _startedBefore; }
		set 
		{
			_startedBefore = value;
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public float? StartedAfter
	{
		get { return _startedAfter; }
		set 
		{
			_startedAfter = value;
			if (_startedBefore != null && _startedAfter != null)
				if (_startedBefore <= _startedAfter)
				{
					_startedAfter = null;
					Debug.LogError ("Incorrect value on minimal starting time stamp");
				}
			if (_endedBefore != null && _startedAfter != null)
				if (_endedBefore <= _startedAfter)
				{
					_startedAfter = null;
					Debug.LogError ("Incorrect value on minimal starting time stamp");
				}
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public float? EndedBefore
	{
		get { return _endedBefore; }
		set 
		{
			_endedBefore = value;
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public float? EndedAfter
	{
		get { return _endedAfter; }
		set 
		{
			_endedAfter = value;
			if (_endedBefore != null && _endedAfter != null)
				if (_endedBefore <= _endedAfter)
				{
					_endedAfter = null;
					Debug.LogError ("Incorrect value on maximum ending time stamp");
				}
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public bool? Antiparticles
	{
		get { return _antiparticles; }
		set
		{
			_antiparticles = value;
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public bool AdditiveFilterType
	{
		get { return _additiveFilterType; }
		set
		{
			_additiveFilterType = value;
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public bool AdditiveFilterFamily
	{
		get { return _additiveFilterFamily; }
		set
		{
			_additiveFilterFamily = value;
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public List<string> FilteredType
	{
		get 
		{ return _filteredType; }
		set
		{
			_filteredType = value;
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}

	public List<ParticleFamily> FilteredFamily
	{
		get { return _filteredFamily; }
		set
		{
			_filteredFamily = value;
			EventRelay.RelayEvent (EventRelay.EventType.FilterChange, this);
		}
	}
	
	public bool[] ViewMode
	{
		get { return _viewMode; }
	}
	
	// Constructors
	public Filter (float minDistMove, float? maxTime)
	{
		MinDistMove = minDistMove;
		MaxTime = maxTime;
	}
	public Filter (float? minLifeSpan, float? maxIniEnergy, bool additiveFilterType, 
		bool additiveFilterFamily, List<string> filteredType, List<ParticleFamily> filteredFamily)
	{
		_minLifeSpan = minLifeSpan;
		_maxIniEnergy = maxIniEnergy;
		_additiveFilterType = additiveFilterType;
		_additiveFilterFamily = additiveFilterFamily;
		_filteredType = filteredType;
		_filteredFamily = filteredFamily;
		_viewMode[(int) ParticleObjects.track] = true;
		_viewMode[(int) ParticleObjects.points] = true;
		_viewMode[(int) ParticleObjects.charge] = true;
	}

	// particle specific info used in CanBeSeen function, 
	// set here so they are not reinitialized every time the function is called
	private float start;
	private float end;
	private float lifeSpan;
	private float iniEnergy;

	// determines whether or not particle should be seen
	public bool CanBeSeen (Particle particle)
	{
		// find relevant information about particle
		start = particle.Point[0].TimeStamp;
		end = particle.Point [particle.Point.Count - 1].TimeStamp;
		lifeSpan = end - start;
		iniEnergy = particle.Point[0].Energy;

		// Verify all filters
		if (_maxLifeSpan != null)
			if (_maxLifeSpan < lifeSpan)
			    return false;

		if (_minLifeSpan != null)
			if (_minLifeSpan > lifeSpan)
				return false;

		if (_maxIniEnergy != null)
			if (_maxIniEnergy < iniEnergy)
				return false;

		if (_minIniEnergy != null)
			if (_minIniEnergy > iniEnergy)
				return false;

		if (_startedBefore != null)
			if (_startedBefore < start)
				return false;

		if (_startedAfter != null)
			if (_startedAfter > start)
				return false;

		if (_endedBefore != null)
			if (_endedBefore < end)
				return false;

		if (_endedAfter != null)
			if (_endedAfter > end)
				return false;

		if (_startedBefore != null)
			if (_startedBefore < start)
				return false;

		if (_antiparticles != null) 
			if (_antiparticles != particle.Type.Antiparticle) 
				return false;

		foreach (string type in _filteredType)
		{
			if ((_additiveFilterType && _filteredType.Count != 0) ^ type == particle.Type.Name)
				return false;
		}
		foreach (ParticleFamily family in _filteredFamily) 
		{
			if ((_additiveFilterFamily && _filteredFamily.Count != 0) ^ family == particle.Type.Family)
				return false;
		}

		// At this point all filters have passed
		return true;
	}

	// Filtering unwanted dataPoints
	public bool LineNotNeeded (DataPointTemp point1, DataPointTemp point2)
	{
		if (Vector3.Distance(point1.Position, point2.Position) < MinDistMove)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public bool LineNotNeeded (DataPointTemp point)
	{
		if (MaxTime != null && point.TimeStamp > MaxTime) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
