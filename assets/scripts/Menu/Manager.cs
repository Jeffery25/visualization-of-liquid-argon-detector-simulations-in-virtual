﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

public class Manager : Singleton<Manager>
{
	/***********************************************
	 * 
	 * Manager of the program. 
	 * Initializes File loading and filters, Event 
	 * showing and toggling, references the particle data 
	 * 
	 **********************************************/
	
	// The dictionary referencing all the data from read files
	private Dictionary<string, DataFileGroup> dataFilesDict = new Dictionary<string, DataFileGroup> ();
	private List<string> dataFilesNames;
	
	// class instances 
	private CurrentEvent currentEvent;
	private DataFile dataFile;
	
	// fields indicating which dataset is currently relevant
	private int _dataFileNum;
	private int _eventNum;
	
	// use if it is necessary to suspend messages 
	public bool messagesSuspend = false;
	
	// Properties
	public int DataFileNum
	{
		get { return _dataFileNum; }
		set 
		{ 
			if (dataFilesNames != null)
			{
				if (0 <= value && value < dataFilesNames.Count)
				{
					_dataFileNum = value;
					UpdateDataFile (value);
					EventNum = 0;
					
					Debug.Log ("File selected " + dataFilesNames[DataFileNum]);
				}
			}
		}
	}
	
	public int EventNum
	{
		get { return _eventNum; }
		set 
		{
			if (dataFile != null)
			{
				if (0 <= value && value < dataFile.events.Count)
				{
					_eventNum = value;
					UpdateEvent (value);
					
					Debug.Log ("Event number " + (_eventNum + 1));
				}
			}
		}
	}

	void Start () 
	{
		gameObject.AddComponent<FilterManage> ();

		// load files and transfer dictionary
		DataFileLoader load = gameObject.AddComponent<DataFileLoader> ();
		dataFilesDict = load.dataFilesDict;
		
		// reference the names in the dictionary
		dataFilesNames = dataFilesDict.Keys.ToList ();
		dataFilesNames.Sort ();

		// populate the hierarchy
		DetectorWireFrame Detector = new DetectorWireFrame ();
		Detector.Show ();
		currentEvent = gameObject.AddComponent<CurrentEvent> ();
		
		// initialize the event
		DataFileNum = 0;
	}

	void Update ()
	{
		if (Input.GetKeyDown (UserControls.prevEvent))
		{
			EventNum --;
		}
		if (Input.GetKeyDown (UserControls.nextEvent))
		{
			EventNum ++;
		}
		if (Input.GetKeyDown (UserControls.prevFile))
		{
			DataFileNum --;
		}
		if (Input.GetKeyDown (UserControls.nextFile))
		{
			DataFileNum ++;
		}
	}

	// The following methods do not use the properties DataFileNum or EventNum, 
	// as they are called in the setters of these very properties.
	private void UpdateDataFile (int dataFileNumber)
	{
		dataFile = dataFilesDict [dataFilesNames [dataFileNumber]].file;
	}

	private void UpdateEvent (int eventNumber)
	{
		if (dataFile != null)
		{
			currentEvent.Particles = dataFile.events[eventNumber].particles;
		}
	}
}
