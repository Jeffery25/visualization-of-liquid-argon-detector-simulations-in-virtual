﻿using UnityEngine;
using System.Collections;

public class UserControls 
{
	/***********************************************
	 * 
	 * All the controls used in the User Interface to 
	 * do something to the scene are defined here
	 * 
	 **********************************************/
	
	// Used in Manager.cs, toggles between files and events within these files
	public static readonly KeyCode nextFile = KeyCode.RightShift;
	public static readonly KeyCode prevFile = KeyCode.LeftShift;
	public static readonly KeyCode nextEvent = KeyCode.RightBracket;
	public static readonly KeyCode prevEvent = KeyCode.LeftBracket;
	
	// Used in CurrentEvent.cs, allows to play, pause, stop
	public static readonly KeyCode playOrPause = KeyCode.Comma;
	public static readonly KeyCode stop = KeyCode.Period;
	
	// Used in FilterManage.cs, changes the current filter in custom ways
	// look only at certain objects
	public static readonly KeyCode toggleCharge = KeyCode.Z;
	public static readonly KeyCode togglePoints = KeyCode.X;
	// filter certain particles
	public static readonly KeyCode filterNeutrons = KeyCode.N;
	public static readonly KeyCode filterPhotons = KeyCode.P;
	public static readonly KeyCode filterNuclei = KeyCode.M;
	public static readonly KeyCode filterElectrons = KeyCode.Q;
	// clear the filtering
	public static readonly KeyCode clear = KeyCode.C;
	// see antiparticles
	public static readonly KeyCode toggleAntiparticles = KeyCode.A;
	public static readonly KeyCode stopToggleAntiparticles = KeyCode.S;
	// see only certain energies
	public static readonly KeyCode higherMaxEnergy = KeyCode.E;
	public static readonly KeyCode lowerMaxEnergy = KeyCode.R;
	// see only certain life spans
	public static readonly KeyCode higherMinLifeSpan = KeyCode.L;
	public static readonly KeyCode lowerMinLifeSpan = KeyCode.Semicolon;
}
