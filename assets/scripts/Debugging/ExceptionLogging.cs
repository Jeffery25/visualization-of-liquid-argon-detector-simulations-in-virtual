﻿using UnityEngine;
using System.Collections;
using System.IO;

public class ExceptionLogging : MonoBehaviour
{
	/***********************************************
	 * 
	 * Writes errors, exceptions and warnings to a 
	 * log file. Taken from Lynda Tutorial Advanced
	 * Unity 3D programming, part 2.
	 * 
	 **********************************************/

	public string saveFile = @"Log.txt";
	private StringWriter logWriter;

	void OnEnable () 
	{
		Application.logMessageReceived += ExceptionWriter;
	}
	
	void OnDisable () 
	{
		// Remove callback when object goes out of scope
		Application.logMessageReceived -= ExceptionWriter;
	}

	//This is an instance of a delegate, we'll cover these in more detail in the Advanced scripting chapter
	//Changing the types or order of the types in this method will break the code.
	//You can name it whatever you like.
	void ExceptionWriter(string logString, string stackTrace, LogType type) 
	{
		switch(type) 
		{
			case LogType.Exception:
			case LogType.Error:
			case LogType.Warning:
				using(StreamWriter writer = new StreamWriter(new FileStream(saveFile, FileMode.Append))) 
				{
					writer.WriteLine(type);
					writer.WriteLine(logString);
					writer.WriteLine(stackTrace);
				}
				break;
			default:
				break;
		}
	}
}
