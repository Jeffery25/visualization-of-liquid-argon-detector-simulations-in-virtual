﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using ProceduralToolkit;

public class DetectorWireFrame 
{
	/***********************************************
	 * 
	 * Draws the Detector in the scane when initialized
	 * in the manager class.
	 *
	 * The detector is made of various parts, themselves 
	 * made of various pieces; different tyes are listed 
	 * in the enums below 
	 * 
	 **********************************************/
	
	// the detector game object
	public static GameObject Detector = new GameObject ();
	private DetectorPart[] parts = new DetectorPart[9];
	
	// visual characteristics of part pieces / material characteristics
	private static Material _detectorMat = new Material (Appearance.detectorMaterial);
	
	// properties
	public static Material DetectorMat { get { return _detectorMat; } }
	
	// List of different parts and pieces included (pieces makes a part which together make the detector)
	private enum PartType
	{
		Cryostat,
		TPC,
	}
	private enum PieceType
	{
		walls,
		edges,	
	}
	
	// Constructor where the main and sub-gameobjects are defined
	public DetectorWireFrame ()
	{
		// the detector game object initialization
		Detector.name = "Detector";
		
		// the initialization of the different detector parts (from geomatry_lbne35t.txt)
		parts[0] = new DetectorPart (PartType.Cryostat, 1, new Vector3 (-58.0914f, -115.264f, -33.6983f), new Vector3 (349.471f, 155.193f, 236.799f));
		parts[1] = new DetectorPart (PartType.TPC, 1, new Vector3 (-35.1844f, -83.77f, -2.03813f), new Vector3 (-6.54525f, 120.794f, 51.4085f));
		parts[2] = new DetectorPart (PartType.TPC, 2, new Vector3 (-1.47915f, -83.77f, -2.03813f), new Vector3 (222.46f, 120.794f, 51.4085f));
		parts[3] = new DetectorPart (PartType.TPC, 3, new Vector3 (-35.1844f, -3.17932f * Mathf.Pow (10, -15), 103.332f), new Vector3 (-6.54525f, -92.8743f, 51.4085f));
		parts[4] = new DetectorPart (PartType.TPC, 4, new Vector3 (-1.47915f, -3.17932f * Mathf.Pow (10, -15), 103.332f), new Vector3 (222.46f, -92.8743f, 51.4085f));
		parts[5] = new DetectorPart (PartType.TPC, 5, new Vector3 (-35.1844f, 0f, 51.4085f), new Vector3 (-6.54525f, 120.794f, 103.332f));
		parts[6] = new DetectorPart (PartType.TPC, 6, new Vector3 (-1.47915f, 0f, 51.4085f), new Vector3 (222.46f, 120.794f, 103.332f));
		parts[7] = new DetectorPart (PartType.TPC, 7, new Vector3 (-35.1844f, -83.77f, 103.332f), new Vector3 (-6.54525f, 120.794f, 156.779f));
		parts[8] = new DetectorPart (PartType.TPC, 8, new Vector3 (-1.47915f, -83.77f, 103.332f), new Vector3 (222.46f, 120.794f, 156.779f));
	}
	
	// methods to show and hide all parts (the full detector)
	public void Show ()
	{
		foreach (DetectorPart part in parts)
		{
			part.Show ();
		}
	}
	
	public void Hide ()
	{
		foreach (DetectorPart part in parts)
		{
			part.Hide ();
		}
	}
	
	// class initializaing the parts listed above
	private class DetectorPart
	{
		// visual rendering and object properties
		private GameObject part = new GameObject ();
		private PartPiece[] pieces = new PartPiece[2];
		
		// Constructor: sets container in hierarchy, makes the pieces
		public DetectorPart (PartType type, int number, Vector3 corner1, Vector3 corner2)
		{
			// set the identity of the part
			part.name = type.ToString () + " " + number.ToString ();
			part.transform.parent = DetectorWireFrame.Detector.transform;
			
			// create the pieces
			pieces[0] = new PartPiece (part, type, PieceType.edges, corner1, corner2);
			pieces[1] = new PartPiece (part, type, PieceType.walls, corner1, corner2);
		}
		
		// method to hide and show the pieces
		public void Show ()
		{
			foreach (PartPiece piece in pieces)
			{
				piece.Show ();
			}
		}
		public void Hide ()
		{
			foreach (PartPiece piece in pieces)
			{
				piece.Hide ();
			}
		}
		
		// wall or edge or additional piece of one part of the detector
		private class PartPiece 
		{
			// visual rendering and object properties
			private GameObject piece = new GameObject ();
			private MeshFilter meshfil;
			private MeshRenderer meshrend;
			
			// Constructor: sets name, mesh, material, container in hierarchy and hides for the moment
			public PartPiece (GameObject container, PartType partType, PieceType pieceType, Vector3 corner1, Vector3 corner2)
			{
				// put the gameobject on a custom layer so that it is not affected by directional light
				piece.layer = 8;
				
				// set the identity of the part
				piece.name = pieceType.ToString ();
				piece.transform.parent = container.transform;
				
				// gameobject components added
				meshfil = piece.AddComponent<MeshFilter> ();
				meshrend = piece.AddComponent<MeshRenderer> ();
				meshrend.material = DetectorWireFrame.DetectorMat;
				meshrend.enabled = false;
				
				// generate approprite mesh
				switch (pieceType)
				{
					// making a wall 
					case PieceType.walls:
						piece.transform.position = (corner1 + corner2) / 2;
						meshfil.mesh = BoxMake (corner1, corner2);
						// picking the color
						switch (partType)
						{
							case PartType.Cryostat:
								meshrend.material.color = Appearance.Col.cryoWall;
								break;
								
							case PartType.TPC:
							meshrend.material.color = Appearance.Col.TPCWall;
								break;
						}
						break;
						
					// making the edges	
					case PieceType.edges:
						meshfil.mesh = WireMake (corner1, corner2);
						// picking the color
						switch (partType)
						{
							case PartType.Cryostat:
								meshrend.material.color = Appearance.Col.cryoEdge;
								break;
								
							case PartType.TPC:
								meshrend.material.color = Appearance.Col.TPCEdge;
								break;
						}
						break;
				}
			}
			
			// use the procedural generation package to generate a box
			private Mesh BoxMake (Vector3 corner1, Vector3 corner2)
			{
				// making the mesh procedurally using width height and length
				Mesh m = new Mesh ();
				Vector3 diagonal = corner2 - corner1;
				m = MeshE.Hexahedron (- Mathf.Abs (diagonal.x), - Mathf.Abs (diagonal.z), - Mathf.Abs (diagonal.y));
				m.RecalculateBounds();
				return m;		
			}
			
			// make the wireframe shaped like a box
			private Mesh WireMake (Vector3 corner1, Vector3 corner2)
			{
				Mesh m = new Mesh();
				int[] indices = GetBoxIndices ();
				m.vertices = GetBoxPoints (corner1, corner2);
				m.SetIndices (indices, MeshTopology.Lines, 0);
				m.RecalculateBounds();
				return m;		
			}
			
			// get vertices of wireframe
			private Vector3[] GetBoxPoints (Vector3 corner1, Vector3 corner2)
			{
				Vector3[] corners = new Vector3[2] {corner1, corner2};
				Vector3[] boxPoints = new Vector3[8];
				
				for (int i = 0; i < 8; i++)
				{
					string binary = Convert.ToString(i, 2);
					while (binary.Length < 3)
					{
						binary = "0" + binary;
					}
					for (int j = 0; j < 3; j++)
					{
						boxPoints[i][j] = corners[(int) binary[j] - '0'][j];
					}
				}
				
				return boxPoints;
			}
			
			// get indices that allow mesh.SetIndices to draw the lines of the wireframe 
			private int[] GetBoxIndices ()
			{
				int counter = 0;
				int increment;
				int[] lineIndices = new int[24];
				bool[] pointUsed = new bool[8];
				
				for (int i = 0; i < 3; i++)
				{
					increment = (int) Mathf.Pow (2, i);
					
					for (int k = 0; k < pointUsed.Length; k++)
					{
						pointUsed[k] = false;
					}
					
					for (int j = 0; j < 8 - increment; j++)
					{
						if (!pointUsed [j] && !pointUsed[j + increment])
						{
							pointUsed[j] = true;
							pointUsed[j + increment] = true;
							lineIndices[counter] = j;
							lineIndices[counter + 1] = j + increment;
							counter += 2;
						}
					} 
				}
				
				return lineIndices;
			}
			
			// the methods to hide, show the piece
			public void Hide ()
			{
				meshrend.enabled = false;
			}
			public void Show ()
			{
				meshrend.enabled  = true;
			}
		}
	}
}
