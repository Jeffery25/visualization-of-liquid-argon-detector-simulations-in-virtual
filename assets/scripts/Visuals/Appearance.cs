﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ProceduralToolkit;

public enum ParticleObjects
{
	/***********************************************
	 * 
	 * List of different objects (tracks, position points, charge points...) 
	 * in particle class. Add here whenever a new visual / gameObject
	 * needs to be added to a Particle. 
	 * 
	 **********************************************/
	
	track,
	points,
	animation,
	charge,
}

public static class Appearance 
{
	/***********************************************
	 * 
	 * Specifies the appearance of the particles in the
	 * visualization, including: all colors, the sizing and scaling
	 * of a point in addition to its precision
	 * 
	 **********************************************/
	
	// number of latitudes / longitudes used to generate a sphere representing a point
	public static int meshPrecision = 10;
	
	/***********************************************
	 * 
	 * Variables for Size of sphere representing each point
	 * 
	 **********************************************/
	
	// the size per unit (GeV, charge etc) in each particle object 
	public static readonly Dictionary<ParticleObjects, float> sizePerUnit = new Dictionary<ParticleObjects, float> ()
	{
		{ ParticleObjects.points, 0.5f },
		{ ParticleObjects.animation, 1f },
		{ ParticleObjects.charge, 0.05f }
	};
	
	// gives points minimum size (so the ones with low energy don't have a radius too close to 0)
	private static readonly Dictionary<ParticleObjects, float> minSizes = new Dictionary<ParticleObjects, float> ()
	{
		{ ParticleObjects.points, 0.5f },
		{ ParticleObjects.animation, 0.5f },
		{ ParticleObjects.charge, 0.5f }
	};
	
	// method that adds or takes away a little amount, to avoid superposed points with exact same radius
	// and avoids the resulting flashing of colors
	private static float noise = 0.05f;
	private static float GetMinSize (ParticleObjects type)
	{
		return minSizes[type] + UnityEngine.Random.Range (-noise, noise);
	}
	
	/***********************************************
	 * 
	 * Scaling into a non linear scale 
	 * (given a linear scale float)
	 * 
	 **********************************************/
	
	// the delegate that scales a given scalar
	public delegate float Scale (ParticleObjects type, float input);
	public static Scale ReScale = Log;
	
	// different scaling functions that can be used.
	// (if we want to use different scaling functions for different 
	// particle objetcs, create a dictionnary like with sizes above)
	public static float Log (ParticleObjects type, float input)
	{
		return Mathf.Log (1 + GetMinSize (type) + input);
	}
	public static float Sqrt (ParticleObjects type, float input)
	{
		return Mathf.Sqrt (GetMinSize (type) + input);
	}
 	
	/***********************************************
	 * 
	 * Material settings for the whole scene
	 * 
	 **********************************************/
 	
 	// Material of Particles 
 	// careful to call new Material (myMaterial), not just myMaterial
	public static Material particleMaterial = new Material (Shader.Find ("Diffuse"));
	public static Material chargeMaterial = new Material (Shader.Find ("Transparent/Diffuse"));
	public static Material detectorMaterial = new Material (Shader.Find ("Transparent/Diffuse"));
 	
	/***********************************************
	 * 
	 * Color settings for the whole scene
	 * 
	 **********************************************/
 	
	// Color Settings for particle "families" that all have the same color
	public static readonly Dictionary<ParticleFamily, Color> ColorDict
		= new Dictionary<ParticleFamily, Color> ()
	{
		{ ParticleFamily.Unknown, Color.white },
		{ ParticleFamily.Nucleus, Color.white },
		{ ParticleFamily.LightMeson, Color.red },
		{ ParticleFamily.StrangeMeson, Color.red },
		{ ParticleFamily.GaugeBoson, Color.grey },
		{ ParticleFamily.StrangeBaryon, ColorE.purple },
	};
	
	// Color settings for particular objects 
	public static class Col
	{
		// color for charge depositions (with transparency)
		public static Color charge = new Color (1f, 1f, 1f, 0.1f);
		
		// colors of the detector pieces (with a given transparency)
		public static Color cryoWall = new Color (1f, 1f, 1f, 0.3f);
		public static Color cryoEdge = new Color (1f, 1f, 1f, 0.8f);
		public static Color TPCWall = new Color (0.1f, 0.8f, 0.1f, 0.1f);
		public static Color TPCEdge = new Color (0.1f, 0.8f, 0.1f, 0.6f);
		
		// particles that have a different color than the rest of their "family"
		public static Color electron = ColorE.blue;
		public static Color muon = Color.cyan;
		public static Color neutrinoEl = ColorE.yellow;
		public static Color neutrinoMu = ColorE.yellow;
		public static Color neutrinoTa = ColorE.yellow;
		public static Color neutron = ColorE.navy;
		public static Color proton = Color.green;
		public static Color tau = Color.magenta;
	}
}