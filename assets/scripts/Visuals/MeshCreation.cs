﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ProceduralToolkit;

public static class MeshCreation 
{
	/***********************************************
	 * 
	 * Contains the methods to make the various meshes 
	 * needed in display.
	 * 
	 **********************************************/
	
	// maximum amount of vertices a mesh can support (Unity default)
	public static int maxVerticesInMesh = 65536;
	
	// properties
	// the number of vertices in a sphere representing each particle position
	public static int VerticesInPointMesh
	{
		get { return Appearance.meshPrecision * (Appearance.meshPrecision + 1); }
	}
	// the maximum amount of spheres representing points that can be put in a single mesh
	public static int maxAmountPoints
	{
		get { return maxVerticesInMesh / VerticesInPointMesh; }
	}
	
	// Methods
	// the number of meshes that need to be generated given a number of positions
	public static int NumberOfMeshes (int numPoints)
	{
		return (numPoints * VerticesInPointMesh) / maxVerticesInMesh + 1;
	}
	
	/***********************************************
	 * 
	 * Methods generating the meshes 
	 * (overloaded, since all take different parameters)
	 * 
	 **********************************************/
	
	// Create a simple point (used fot the animation for example)
	public static Mesh CreateMesh (ParticleObjects type)
	{	
		return MeshE.Sphere (Appearance.sizePerUnit[type], Appearance.meshPrecision, Appearance.meshPrecision);
	}
	
	// line mesh (connect the dots with all data point positions)
	public static Mesh CreateMesh (Vector3[] positions)
	{
		Mesh m = new Mesh ();
		
		if (NumberOfMeshes (positions.Length) != 1)
		{
			Debug.LogError ("CreateMesh cannot make a mesh with " + positions.Length.ToString () + 
			                " positions, as each position takes " + VerticesInPointMesh.ToString () + " vertices and the maximum is " 
			                + maxVerticesInMesh.ToString () + " vertices."); 
			return m;
		}
		
		int[] indices = new int [positions.Length];
		
		for (int i = 0; i < indices.Length; i++)
		{
			indices[i] = i;
		}
		
		m.vertices = positions;
		m.SetIndices (indices, MeshTopology.LineStrip, 0);
		m.RecalculateBounds ();
		
		return m;
	}
	
	// all data points as spheres in one mesh
	public static Mesh CreateMesh (ParticleObjects type, Vector3[] positions, float[] scales)
	{
		Mesh m = new Mesh ();
		
		if (positions.Length != scales.Length)
		{
			Debug.LogError ("CreateMesh (Vector3[] positions, float[] energies) should take in arrays of the same length");
			return m;
		}
		if (NumberOfMeshes (positions.Length) != 1)
		{
			Debug.LogError ("CreateMesh cannot make a mesh with " + positions.Length.ToString () + 
				" positions, as each position takes " + VerticesInPointMesh.ToString () + " vertices and the maximum is " 
				+ maxVerticesInMesh.ToString () + " vertices."); 
			return m;
		}
		
		CombineInstance[] combine = new CombineInstance[positions.Length];
		Mesh template = CreateMesh (type);

		for (int j = 0; j < positions.Length; j++) 
		{
			Matrix4x4 mx = Matrix4x4.identity * Matrix4x4.Scale (Appearance.ReScale (type, scales[j]) * Vector3.one);
			mx.SetColumn (3, new Vector4 (positions[j].x, positions[j].y, positions[j].z));
			combine[j].mesh = template;
			combine[j].transform = mx;
		}
		
		m.CombineMeshes (combine);
		m.RecalculateBounds();
		
		return m;
	}
	
	// combines the two givens meshes into one (currently not in use)
	public static Mesh Combine (Mesh one, Mesh two)
	{
		Mesh m = new Mesh ();
		CombineInstance[] combine = new CombineInstance [2];
		combine [0].mesh = one;
		combine [1].mesh = two;
		m.CombineMeshes (combine, false, false);
		return m;
	}
	
}
